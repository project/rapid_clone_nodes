<?php

namespace Drupal\rapid_clone_nodes\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;

/**
 * Clone nodes bulk operations listing page.
 *
 * @Action(
 *   id = "rapid_clone_nodes_clone_node_action",
 *   label = @Translation("Rapid clone nodes"),
 *   type = "node",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "Rapid clone nodes: Manage access of clone node listing
 *     page",
 *     "_custom_access" = TRUE,
 *   },
 * )
 */
class CloneNodeAction extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $node = Node::load($entity->id());

    if (is_object($node)) {
      $clonednode = $node->createDuplicate();
      // Loop over entity fields and duplicate nested paragraphs.
      foreach ($clonednode->getFields() as $field) {
        if ($field->getFieldDefinition()->getType() == 'entity_reference_revisions') {
          if ($field->getFieldDefinition()->getFieldStorageDefinition()->getSetting('target_type') == "paragraph") {
            $paragraphs = [];
            foreach ($field as $item) {
              $paragraphs[] = $item->entity->createDuplicate();
            }
            $fieldname = $field->getFieldDefinition()->getName();
            $clonednode->$fieldname = $paragraphs;
          }
        }
      }

      $clonednode->changed->value = time();

      // Append cloned in the title.
      $clonednode->set('title', 'Cloned - ' . $node->getTitle());

      // Cloned node should be unpublished.
      $clonednode->set('status', 0);

      $clonednode->save();

      return $this->t('No. of cloned nodes.');
    }
    else {
      return $this->t('No nodes cloned.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // If certain fields are updated, access should be checked against
    // them as well.
    // @see Drupal\Core\Field\FieldUpdateActionBase::access().
    return $object->access('update', $account, $return_as_object);
  }

}
